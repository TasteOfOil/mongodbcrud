const Lesson = require('./schema');

const createLesson = async (req,res)=>{
    try{
        const lesson = await Lesson.create(req.body);
        res.status(201).json({lesson});
        console.log('Create lesson');
    }
    catch(err){
        res.status(500).json({msg:err});
    }
    
}

const getLessons = async (req,res)=>{
    try{
        const lessons = await Lesson.find({});
        res.status(200).json({lessons});
        console.log('Get all lessons');
    }
    catch(err){
        res.status(500).json({msg:err});
    }
    
}

const getLesson = async(req,res)=>{
    try{
        const lesson = await Lesson.findOne({_id:req.params.id});

        if(!lesson){
            return res.status(404).json({msg:`No lesson with id: ${req.params.id}`});
        }

        res.status(200).json({lesson});
        console.log('Get lesson by id');
    }
    catch(err){
        res.status(500).json({msg:err});
    }
}



const deleteLesson = async(req, res)=>{
    try{
        const lesson = await Lesson.findOneAndDelete({_id:req.params.id});
        if(!lesson){
            return res.status(404).json({msg:`No lessons with id: ${req.params.id}`});
        }
        res.status(200).json({lesson});
        console.log('Delete lesson');
    }
    catch(err){
        res.status(500).json({msg:err});
    }
}

const updateLesson = async(req,res)=>{
    try{
        const lesson = await Lesson.findOneAndUpdate({_id:req.params.id}, req.body, {
            new:true
        });
        if(!lesson){
            return res.status(404).json({msg:`No lessons with id: ${req.params.id}`});
        }
        
        res.status(200).json({lesson});
        console.log('Update lesson');
    }
    catch(err){
        res.status(500).json({msg:err});
    }
} 

module.exports = {createLesson, getLessons,
    getLesson, deleteLesson, updateLesson}