const mongoose = require('mongoose');

const LessonSchema = new mongoose.Schema({
    lessonName:{
        type:String,
        required:true
    },
    teacherName:{
        type:String,
        required:true
    },
    countStudents:{
        type:Number,
        required:true
    }
})

module.exports = mongoose.model('Lesson', LessonSchema);