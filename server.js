const connectDB = require('./mongooseConnect');
const lessonsCRUD = require('./lessonsCRUD');
const cors = require('cors');
const express = require('express');
const app = express();
require('dotenv').config();
const port = process.env.PORT||3000;

const start = async ()=>{
    try{
        await connectDB(process.env.MONGO_URL);
        app.listen(port, ()=>{
            console.log(`listening on port:${port}`);
        });
    } catch(err){
        console.log(err);
    }

}

start();

app.use(express.json());
app.use(cors({
    origin:'*'
}));




app.get('/',(req,res)=>{
    res.send("It's start page");
});

app.get('/api/lessons',lessonsCRUD.getLessons);

app.get('/api/lessons/:id', lessonsCRUD.getLesson);

app.post('/api/lessons/',lessonsCRUD.createLesson);

app.put('/api/lessons/:id', lessonsCRUD.updateLesson);

app.delete('/api/lessons/:id', lessonsCRUD.deleteLesson);
